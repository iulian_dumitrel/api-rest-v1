<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Rutas registro - login
Route::post("users/register", "\App\Http\Controllers\UserController@store");
Route::post("users/login", "\App\Http\Controllers\UserController@login");

// Ruta interacción api.
Route::apiResource("v1/posts", \App\Http\Controllers\Api\V1\PostController::class);


