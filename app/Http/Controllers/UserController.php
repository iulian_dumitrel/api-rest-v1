<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function store(Request $request){
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => \Hash::make($request->password)
        ]);
        if(!is_null($user)){
            return response()->json([
                "res" => true,
                "message" => "Usuario creado correctamente",
            ], 201);
        }else{
            return response()->json([
                "res" => false,
                "message" => "Error al crear el usuario",
            ], 200);
        }
    }

    public function login(Request $request){
        $user = User::whereEmail($request->email)->first();
        if(!is_null($user) && \Hash::check($request ->password, $user->password)){
            // Cuando se logea un usuario por primera vez, se genera un token y se guarda en la tabla. (luego este será el que se manda en la Cabecera como "Authorization" -> "Bearer <token>")
            $user->api_token = \Str::random(100);
            $user->save();

            return response()->json([
                "res" => true,
                "message" => "Usuario logeado correctamente",
                "token" => $user->api_token,
            ], 200);
        }else{
            return response()->json([
                "result" => false,
                "message" => "Error en las credenciales",
            ], 200);
        }
    }
}
