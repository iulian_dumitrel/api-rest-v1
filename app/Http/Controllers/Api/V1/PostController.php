<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use  App\Http\Requests\V1\PostRequest;
use App\Models\Post;
use App\Models\User;

class PostController extends Controller{

    // Se necesitará pasar el middleware para poder manejar los metodos de escritura.
    // Si no solo se podrá tener permisos de lectura
    public function __construct(){
        $this->middleware("auth:api", ["except" => ["index","show"] ]);
    }

    public function index()
    {
        $data = Post::all();
        return $data;
    }

    public function show(Post $post)
    {
        $post = Post::find($post);
        if(!is_null($post)){
            return $post;
        }else{
            return response()->json([
                "message" => "No ha sido posible encontrar el post con la id $post",
            ], 200);
        }

    }

    public function store(PostRequest $request){
        $user = User::find(2);

        $post = new Post();
        $post->user()->associate($user);
        $post->title = $request->input('title');
        $post->post_message = $request->input('post_message');

        $res = $post->save();

        if ($res) {
            return response()->json(['message' => 'Post create succesfully'], 201);
        }
        return response()->json(['message' => 'Error to create post'], 200);
    }

    public function update(Request $request, $post)
    {
        $post = Post::find($post);
        if($request->title != "") $post->title = $request->title;
        if($request->post_message != "") $post->post_message = $request->post_message;
        $res = $post->save();
        if($res){
            return response()->json(['message' => 'Post modificado con éxito'], 201);
        }else{
            return response()->json(["message" => "Error al modificar el post"], 200);
        }

    }

    public function destroy(Post $post){
        if($post->delete()){
            return response()->json(['message' => 'Post eliminado con éxito'], 201);
        }else{
            return response()->json(["message" => "Error al eliminar el post"], 200);
        }
    }
}
