## API REST BÁSICA

Sirve para realizar todas las operaciones CRUD de la tabla "posts"
	- También se puede crear usuarios y loguearse

# How it works?

- Primero hay que hacer un migrate en la database para que se suban las tablas "users" y "posts".
- Posteriormente hay que crear un usuario mandando los datos "name", "email", y "password" (/api/users/register)
- Login con usuario creado, enviando el "email" y "password" (/api/users/login)

- Ahora que se hay login, se crea un token y se guarda en la tabla users, ese mismo se utilizara en headers para las operaciones que requieran estar autentificado mediante el middleware auth:api que hay en el controlador de Users.
- En el controlador de Post, tiene un constructor dónde ejecuta Middleware para todos los metodos excepto "index y show".
- (/api/v1/posts) es dónde se encuentra la url, depende del Verbo , hara x o y acción.
